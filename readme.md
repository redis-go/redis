# redis

[![GoDoc](https://pkg.go.dev/badge/gitee.com/redis-go/redis)](https://pkg.go.dev/gitee.com/redis-go/redis)

Redis is a [Go](http://golang.org/) client for the [Redis](http://redis.io/) database.

这个库是复制 [gomodule/redigo](https://github.com/gomodule/redigo), 库信息请移步到源库查看

## 同步信息

```text
提交编号: d6854479365f0307560fa28e18e2bd0634b05229
提交标签: v1.8.9
显示标签: v1.8.9
提交时间: 2022-07-06 12:30:13 +0100
同步时间: Sun, 25 Sep 2022 02:53:30 +0800
```

## License

Redigo is available under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).
